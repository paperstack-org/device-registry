# Device registry

A microservice to manage device information, such as connectivity information and client certificates.

The API documentation can be found at [https://paperstack-org.gitlab.io/device-registry](https://paperstack-org.gitlab.io/device-registry).

## Usage

Start out by creating a `.env` file with the following contents:

```ini
# The port for the HTTP server.
PORT=8080
# The connection string for the MongoDB database.
MONGO_URI=mongodb://mongodb:mongodb@mongodb:27017/device-registry?authSource=admin&ssl=false
# The trusted service domain.
ROOT_DOMAIN=localtest.me
# The directory that contains the certificates.
CERT_DIR=/workspaces/device-registry/certs
# The application environment it is run in. This can be either "localhost" or "production". Defaults to "production".
ENVIRONMENT=localhost
```

Ensure that you have at least `go1.13` and `build-essential` installed. The run the following commands:

```bash
$ make certs
$ make
```

To build the binary, run: `make build`

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
