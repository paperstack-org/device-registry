# Define build image.
FROM golang:alpine AS build

# Define version build argument.
ARG VERSION

# Define working directory.
WORKDIR /app

# Install build tools.
RUN if [ "$(uname -m)" = "x86_64" ]; then apk add --no-cache make upx; else apk add --no-cache make; fi

# Copy source files.
COPY . .

# Compile binary.
RUN if [ "$(uname -m)" = "x86_64" ]; then VERSION=$VERSION ARCH=amd64 make build; mv device-registry-amd64 device-registry; fi
RUN if [ "$(uname -m)" = "armv7l" ]; then VERSION=$VERSION ARCH=armv7 make build; mv device-registry-armv7 device-registry; fi
RUN if [ "$(uname -m)" = "aarch64" ]; then VERSION=$VERSION ARCH=arm64 make build; mv device-registry-arm64 device-registry; fi

# Define runtime image.
FROM alpine AS run

# Configure non-root user.
RUN adduser --disabled-password app
USER app:app

# Configure working directory.
WORKDIR /app

# Copy binary.
COPY --chown=app:app --from=build /app/device-registry .

# Define environment variables.
ENV PORT=
ENV MONGO_URI=
ENV ROOT_DOMAIN=
ENV CERT_DIR=
ENV ENVIRONMENT=

# Run binary.
CMD ./device-registry
