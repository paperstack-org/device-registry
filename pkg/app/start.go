package app

import (
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/helmet/v2"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/paperstack-org/device-registry/pkg/config"
	"gitlab.com/paperstack-org/device-registry/pkg/middlewares"
	"gitlab.com/paperstack-org/device-registry/pkg/routers"
	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

// Start starts the application.
func Start() {
	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	})

	cfg := config.Load()

	log.Info().Msg("Service: " + cfg.Service)
	log.Info().Msg("Version: " + cfg.Version)

	// Connect to database.
	util.Connect()
	log.Info().Msg("Connected to database: " + cfg.Database)

	// Write service token.
	util.WriteServiceToken()

	// Create router.
	app := fiber.New(fiber.Config{
		ErrorHandler:          middlewares.Error(),
		DisableStartupMessage: true,
	})

	// Load middlewares.
	app.Use(recover.New())
	app.Use(helmet.New())
	app.Use(cors.New(cors.Config{
		AllowHeaders:     "Accept,Authorization,Content-Typem,X-CSRF-Token",
		AllowCredentials: true,
		MaxAge:           600,
	}))
	app.Use(compress.New(compress.Config{
		Level: compress.LevelBestCompression,
	}))
	app.Use(middlewares.RedirectSlashes())
	app.Use(middlewares.ContentType())
	app.Use(middlewares.AuthNJWT())

	// Mount routers and handlers.
	app.Mount("/hardware-platforms", routers.HardwarePlatform())
	app.Mount("/health", routers.Health())

	// Configure fallback route.
	app.Use(middlewares.NotFound())

	log.Info().Msg("Service online: " + cfg.ServiceURI + "/health")
	if err := app.Listen(":" + cfg.Port); err != nil {
		log.Fatal().Err(err).Msg("Service failed")
	}
}
