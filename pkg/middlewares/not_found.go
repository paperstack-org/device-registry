package middlewares

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

// NotFound returns a middlware for endpoints that do not exist.
func NotFound() func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		return util.SendError(c, &util.ReasonEndpointUnsupported)
	}
}
