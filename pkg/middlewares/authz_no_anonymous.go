package middlewares

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

// AuthZNoAnonymous rejects requests from anonymous clients.
func AuthZNoAnonymous() func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		if c.Locals("role") == util.RoleAnonymous {
			return util.SendError(c, &util.ReasonCredentialsMissing)
		}

		return c.Next()
	}
}
