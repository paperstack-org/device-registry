package middlewares

import (
	"encoding/json"
	"net/http"
	"reflect"

	"go.mongodb.org/mongo-driver/mongo"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"

	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

// Error returns a middleware to handler errors during requests.
func Error() func(*fiber.Ctx, error) error {
	return func(c *fiber.Ctx, err error) error {
		// Handle errors if no document can be found.
		if err == mongo.ErrNoDocuments {
			return util.SendError(c, &util.ReasonEntityUnknown)
		}

		// Handle duplicates during document creation.
		if merr, ok := err.(mongo.WriteException); ok && merr.WriteErrors[0].Code == 11000 {
			return util.SendError(c, &util.ReasonEntityExists)
		}

		// Handle duplicates during document updates.
		if merr, ok := err.(mongo.CommandError); ok && merr.Code == 11000 {
			return util.SendError(c, &util.ReasonEntityExists)
		}

		// Handle JSON parsing errors.
		if _, ok := err.(*json.SyntaxError); ok {
			return util.SendError(c, &util.ReasonJSONInvalid)
		}

		// Handle validation errors.
		if _, ok := err.(validator.ValidationErrors); ok {
			return util.SendError(c, &util.ReasonValidationFailed)
		}

		// Handle custom fiber errors.
		if ferr, ok := err.(*fiber.Error); ok {
			reason := util.NewReason(http.StatusInternalServerError, ferr.Message)
			log.Error().Msg(reason.Reason)
			return util.SendError(c, &reason)
		}

		// Display error and error type if the error is unexpected.
		log.Error().Err(err).Str("error_type", reflect.TypeOf(err).String()).Msg(util.ReasonUnexpectedError.Reason)
		return util.SendError(c, &util.ReasonUnexpectedError)
	}
}
