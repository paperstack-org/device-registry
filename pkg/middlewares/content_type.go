package middlewares

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/paperstack-org/device-registry/pkg/config"
)

// ContentType is a middleware to set the "Content-Type" header.
func ContentType() func(*fiber.Ctx) error {
	cfg := config.Load()

	return func(c *fiber.Ctx) error {
		c.Request().Header.Set("Content-Type", cfg.ContentType)
		return c.Next()
	}
}
