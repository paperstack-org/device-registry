package middlewares

import (
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"net/http"
	"net/url"
	"strings"

	"github.com/cristalhq/jwt/v3"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/paperstack-org/device-registry/pkg/config"
	"gitlab.com/paperstack-org/device-registry/pkg/models"
	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

// AuthNJWT is a middleware to authenticate clients via a JWT passed in
// via the "Authorization" header with the "Bearer" scheme.
func AuthNJWT() func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		token := ExtractBearerTokenFromAuthorizationHeader(c)
		if token == nil {
			c.Locals("role", util.RoleAnonymous)
			return c.Next()
		}

		// Get the service claims from the token.
		claims := ValidateServiceClaims(token)
		if claims == nil {
			return util.SendError(c, &util.ReasonCredentialsInvalid)
		}

		// Validate the token signature.
		if !ValidateTrustedServiceSignature(token, claims) {
			return util.SendError(c, &util.ReasonCredentialsInvalid)
		}

		c.Locals("role", util.RoleTrustedService)
		return c.Next()
	}
}

// ExtractBearerTokenFromAuthorizationHeader extracts a token with the "bearer"
// scheme from the "Authorization" HTTP header.
func ExtractBearerTokenFromAuthorizationHeader(c *fiber.Ctx) *jwt.Token {
	authHeader := string(c.Request().Header.Peek("Authorization"))
	authSegments := strings.Split(authHeader, " ")

	// The client did not provide any credentials.
	if len(authHeader) == 0 {
		return nil
	}

	// The client passed invalid credentials into the "Authorization" header.
	if len(authSegments) != 2 || strings.ToLower(authSegments[0]) != "bearer" {
		return nil
	}

	// Parse JWT token.
	token, err := jwt.ParseString(authSegments[1])
	if err != nil {
		return nil
	}

	return token
}

// ValidateServiceClaims will validate some basic claims of the
// JWT. Upon successful validation the method will return the claims
// or nil if the validation fails.
func ValidateServiceClaims(token *jwt.Token) *util.ServiceClaims {
	cfg := config.Load()

	// Check if the algorithm is either "RS512" or "ES512".
	algorithm := token.Header().Algorithm
	if algorithm != jwt.RS512 && algorithm != jwt.ES512 {
		return nil
	}

	// Parse JWT claims.
	claims := new(util.ServiceClaims)
	if err := json.Unmarshal(token.RawClaims(), claims); err != nil {
		return nil
	}

	// Check audience.
	if !claims.IsForAudience(cfg.ServiceURI) {
		return nil
	}

	// Check if the identity type is allowed.
	if claims.IdentityType != util.IdentityTypeTrustedService {
		return nil
	}

	return claims
}

// ValidateTrustedServiceSignature will validate the service
// signature of a trusted service. As first step the domain
// name will be verified in the claims if it matches the trusted
// domain. Only after that the public key will be fetched then
// used to validate the signature.
func ValidateTrustedServiceSignature(token *jwt.Token, claims *util.ServiceClaims) bool {
	cfg := config.Load()

	// Parse issuer URL.
	issuerURL, err := url.Parse(claims.Issuer)
	if err != nil {
		return false
	}

	// Validate additional security parameters in a production environment.
	if cfg.Environment == config.EnvironmentProduction {
		if issuerURL.Scheme != "https" || issuerURL.Port() != "" {
			return false
		}

		// Check if the service domain is trusted.
		if !strings.HasSuffix(issuerURL.Host, "."+cfg.RootDomain) {
			return false
		}
	} else {
		// Check if the service domain is trusted.
		if !strings.HasSuffix(issuerURL.Hostname(), "."+cfg.RootDomain) {
			return false
		}
	}

	// Get the service identities public key.
	res, err := http.Get(claims.Issuer + "/health")
	if err != nil {
		return false
	}

	// Parse response body.
	health := new(models.HealthResponse)
	if err := json.NewDecoder(res.Body).Decode(health); err != nil {
		return false
	}

	// Parse public key.
	publicKeyBlock, _ := pem.Decode([]byte(health.Data.Identity.PublickKey))
	publicKey, err := x509.ParsePKIXPublicKey(publicKeyBlock.Bytes)
	if err != nil {
		return false
	}

	// Create new verifier.
	var verifier jwt.Verifier
	switch health.Data.Identity.Format {
	case config.IdentityFormatECPrime256V1PEM:
		ecdsaPublicKey, ok := publicKey.(*ecdsa.PublicKey)
		if !ok {
			return false
		}

		verifier, err = jwt.NewVerifierES(jwt.ES512, ecdsaPublicKey)
		if err != nil {
			return false
		}
	case config.IdentityFormatRSAPEM:
		rsaPublicKey, ok := publicKey.(*rsa.PublicKey)
		if !ok {
			return false
		}

		verifier, err = jwt.NewVerifierRS(jwt.RS512, rsaPublicKey)
		if err != nil {
			return false
		}
	default:
		return false
	}

	// Verify token signature.
	return verifier.Verify(token.Payload(), token.Signature()) == nil
}
