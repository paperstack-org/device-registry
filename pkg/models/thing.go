package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Thing is a connected device.
type Thing struct {
	OID                  primitive.ObjectID `json:"-" bson:"_id,omitempty"`
	ID                   string             `json:"id" bson:"id,omitempty"`
	HardwarePlatformHref string             `json:"hardware_platform_href" bson:"hardware_platform_href,omitempty"`
	SerialNumber         string             `json:"serial_number" bson:"serial_number,omitempty"`
	OperatingSystem      string             `json:"operating_system" bson:"operating_system,omitempty"`
}

// ThingIndexes describes the indexes for the model.
var ThingIndexes = []mongo.IndexModel{
	{
		Keys:    bson.D{bson.E{Key: "id", Value: 1}},
		Options: options.Index().SetUnique(true),
	},
	{
		Keys: bson.D{
			bson.E{Key: "hardware_platform_href", Value: 1},
			bson.E{Key: "serial_number", Value: 1},
		},
		Options: options.Index().SetUnique(true),
	},
}
