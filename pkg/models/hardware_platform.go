package models

import (
	"reflect"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var hardwarePlatformValidate = validator.New()

// HardwarePlatform is the board and chip configuration of an IoT device.
type HardwarePlatform struct {
	OID             primitive.ObjectID `json:"-" bson:"_id,omitempty" validate:"-"`
	ID              string             `json:"id" bson:"id,omitempty" validate:"uuid4"`
	TargetName      string             `json:"target_name" bson:"target_name,omitempty" validate:"required,lowercase,hostname,excludesrune=."`
	Platform        string             `json:"platform" bson:"platform,omitempty" validate:"required"`
	Model           string             `json:"model" bson:"model,omitempty" validate:"required"`
	CPUArchitecture string             `json:"cpu_architecture" bson:"cpu_architecture,omitempty" validate:"required,lowercase"`
	CreatedAt       string             `json:"created_at" bson:"created_at,omitempty" validate:"required"`
}

// HardwarePlatformIndexes describes the indexes for the model.
var HardwarePlatformIndexes = []mongo.IndexModel{
	{
		Keys:    bson.D{bson.E{Key: "id", Value: 1}},
		Options: options.Index().SetUnique(true),
	},
	{
		Keys:    bson.D{bson.E{Key: "target_name", Value: 1}},
		Options: options.Index().SetUnique(true),
	},
	{
		Keys: bson.D{
			bson.E{Key: "platform", Value: 1},
			bson.E{Key: "model", Value: 1},
			bson.E{Key: "cpu_architecture", Value: 1},
		},
		Options: options.Index().SetUnique(true),
	},
	{
		Keys: bson.D{
			bson.E{Key: "created_at", Value: -1},
			bson.E{Key: "_id", Value: -1},
		},
	},
}

// Validate will trim strings and validate the model.
func (hp *HardwarePlatform) Validate(new bool) error {
	// If the entity is new, set a UUID and creation time.
	if new {
		hp.ID = uuid.New().String()
		hp.CreatedAt = time.Now().UTC().Format(time.RFC3339)
	}

	// Trim all leading and trailing white space of all strings.
	value := reflect.ValueOf(hp).Elem()
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)

		// Trim only strings.
		if field.Type() == reflect.TypeOf("") {
			field.SetString(strings.TrimSpace(field.Interface().(string)))
		}
	}

	// Validate entity.
	if err := hardwarePlatformValidate.Struct(hp); err != nil {
		return err
	}

	return nil
}
