package routers

import (
	"net/http"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/paperstack-org/device-registry/pkg/config"
	"gitlab.com/paperstack-org/device-registry/pkg/middlewares"
	"gitlab.com/paperstack-org/device-registry/pkg/models"
	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

var health *models.Health

// ListHealth displays application server information.
func ListHealth(c *fiber.Ctx) error {
	return c.Status(http.StatusOK).JSON(util.DataBody{
		Data: *health,
	})
}

// Health returns a router for the service.
func Health() *fiber.App {
	cfg := config.Load()
	router := fiber.New(fiber.Config{
		ErrorHandler: middlewares.Error(),
	})

	// Cache application health.
	health = new(models.Health)
	*health = models.Health{
		Version:   cfg.Version,
		Hostname:  cfg.Hostname,
		StartedAt: cfg.StartedAt,
		Identity:  cfg.ServiceIdentity,
	}

	// Configure route handlers.
	router.Get("/", ListHealth)

	return router
}
