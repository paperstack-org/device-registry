package routers

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/paperstack-org/device-registry/pkg/middlewares"
	"gitlab.com/paperstack-org/device-registry/pkg/models"
	"gitlab.com/paperstack-org/device-registry/pkg/util"
)

var hardwarePlatforms = []models.HardwarePlatform{
	{
		ID:              "26086453-a12b-4cfe-b155-0237d141826b",
		TargetName:      "esp32-devkitc",
		Platform:        "ESP32",
		Model:           "DevKitC",
		CPUArchitecture: "xtensa",
		CreatedAt:       time.Now().UTC().Format(time.RFC3339),
	},
	{
		ID:              "9f2c2358-49f5-4077-bffc-429eda21ea54",
		TargetName:      "esp32-wroverkit",
		Platform:        "ESP32",
		Model:           "WROVER-KIT",
		CPUArchitecture: "xtensa",
		CreatedAt:       time.Now().UTC().Format(time.RFC3339),
	},
	{
		ID:              "18eb5785-1c89-4536-8180-2c05efb1fc2f",
		TargetName:      "esp32-picokit",
		Platform:        "ESP32",
		Model:           "PICO-KIT",
		CPUArchitecture: "xtensa",
		CreatedAt:       time.Now().UTC().Format(time.RFC3339),
	},
	{
		ID:              "ced9cc1b-5243-4181-90fe-89011ffef18a",
		TargetName:      "raspberrypi-3bplus",
		Platform:        "Raspberry Pi",
		Model:           "3B+",
		CPUArchitecture: "armv7",
		CreatedAt:       time.Now().UTC().Format(time.RFC3339),
	},
}

type manyResponse struct {
	Data       []models.HardwarePlatform `json:"data"`
	Pagination util.Pagination           `json:"pagination"`
}

type oneResponse struct {
	Data models.HardwarePlatform `json:"data"`
}

type errorResponse struct {
	Error util.Reason `json:"error"`
}

var app *fiber.App
var url = "/hardware-platforms"

func TestMain(m *testing.M) {
	// Copy ".env" file.
	filename := ".env"

	data, err := ioutil.ReadFile("../../" + filename)
	if err != nil {
		log.Fatal(err)
	}

	if err := ioutil.WriteFile(filename, data, 0644); err != nil {
		log.Fatal(err)
	}

	_, err = util.Connect().Collection("hardware-platforms").DeleteMany(context.Background(), bson.D{})
	if err != nil {
		log.Fatal(err)
	}

	app = fiber.New(fiber.Config{
		ErrorHandler: middlewares.Error(),
	})
	app.Use(middlewares.ContentType())
	app.Mount("/hardware-platforms", HardwarePlatform())

	rc := m.Run()

	if err := os.Remove(filename); err != nil {
		log.Fatal(err)
	}

	os.Exit(rc)
}

func TestListHardwarePlatformsWithDefaults(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	var ids []primitive.ObjectID
	for i := len(hardwarePlatforms); i > 0; i-- {
		doc, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[i-1])
		assert.Nil(err)
		ids = append(ids, doc.InsertedID.(primitive.ObjectID))
	}

	// Act.
	req, err := http.NewRequest(http.MethodGet, url, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body manyResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(manyResponse{
		Data: hardwarePlatforms,
		Pagination: util.Pagination{
			Next:  ids[0].Hex(),
			Limit: 20,
		},
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestListHardwarePlatformsWithLimit(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	var ids []primitive.ObjectID
	for i := range hardwarePlatforms {
		doc, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[i])
		ids = append(ids, doc.InsertedID.(primitive.ObjectID))
		assert.Nil(err)
	}

	// Act.
	req, err := http.NewRequest(http.MethodGet, url+"?limit=1", nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body manyResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(manyResponse{
		Data: []models.HardwarePlatform{hardwarePlatforms[len(hardwarePlatforms)-1]},
		Pagination: util.Pagination{
			Next:  ids[len(hardwarePlatforms)-1].Hex(),
			Limit: 1,
		},
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestListHardwarePlatformsWithNext(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	var ids []primitive.ObjectID
	for i := range hardwarePlatforms {
		doc, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[i])
		ids = append(ids, doc.InsertedID.(primitive.ObjectID))
		assert.Nil(err)
	}

	// Act.
	req, err := http.NewRequest(http.MethodGet, url+"?next="+ids[2].Hex()+"&limit=1", nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body manyResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(manyResponse{
		Data: []models.HardwarePlatform{hardwarePlatforms[1]},
		Pagination: util.Pagination{
			Next:  ids[1].Hex(),
			Limit: 1,
		},
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestCreateHardwarePlatformWithSuccess(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	payloadBytes, err := json.Marshal(hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusCreated, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: hardwarePlatforms[0]}
	expectedBody.Data.ID = body.Data.ID
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: body.Data.ID}}).Decode(&doc)
	doc.OID = primitive.ObjectID{}
	assert.Nil(err)
	assert.Equal(expectedBody.Data, doc)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestCreateHardwarePlatformWithJsonInvalid(t *testing.T) {
	// Arrange.
	assert := assert.New(t)

	// Act.
	req, err := http.NewRequest(http.MethodPost, url, strings.NewReader("{key:value}"))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusBadRequest, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonJSONInvalid,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestCreateHardwarePlatformWithValidationFailed(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	invalidHardwarePlatform := hardwarePlatforms[0]
	invalidHardwarePlatform.TargetName = "ESP32"
	payloadBytes, err := json.Marshal(invalidHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusBadRequest, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonValidationFailed,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestCreateHardwarePlatformWithConflict(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)
	payloadBytes, err := json.Marshal(hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusConflict, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonEntityExists,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestCreateHardwarePlatformWithTrimming(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	untrimmedHardwarePlatform := hardwarePlatforms[0]
	untrimmedHardwarePlatform.TargetName = "    unicorn    "
	payloadBytes, err := json.Marshal(untrimmedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusCreated, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: untrimmedHardwarePlatform}
	expectedBody.Data.TargetName = "unicorn"
	expectedBody.Data.ID = body.Data.ID
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: body.Data.ID}}).Decode(&doc)
	doc.OID = primitive.ObjectID{}
	assert.Nil(err)
	assert.Equal(expectedBody.Data, doc)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestReadHardwarePlatformWithID(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodGet, url+"/"+hardwarePlatforms[0].ID, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: hardwarePlatforms[0]}
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestReadHardwarePlatformWithTargetName(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodGet, url+"/"+hardwarePlatforms[0].TargetName, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: hardwarePlatforms[0]}
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestReadHardwarePlatformWithEntityUnknown(t *testing.T) {
	// Arrange.
	assert := assert.New(t)

	// Act.
	req, err := http.NewRequest(http.MethodGet, url+"/"+hardwarePlatforms[0].TargetName, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusNotFound, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonEntityUnknown,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithID(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)
	updatedHardwarePlatform := hardwarePlatforms[0]
	updatedHardwarePlatform.Model = "ESP32-S0"
	payloadBytes, err := json.Marshal(updatedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[0].ID, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: updatedHardwarePlatform}
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: body.Data.ID}}).Decode(&doc)
	doc.OID = primitive.ObjectID{}
	assert.Nil(err)
	assert.Equal(expectedBody.Data, doc)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithTargetName(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)
	updatedHardwarePlatform := hardwarePlatforms[0]
	updatedHardwarePlatform.Model = "ESP32-S0"
	payloadBytes, err := json.Marshal(updatedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[0].TargetName, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: updatedHardwarePlatform}
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: body.Data.ID}}).Decode(&doc)
	doc.OID = primitive.ObjectID{}
	assert.Nil(err)
	assert.Equal(expectedBody.Data, doc)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithJsonInvalid(t *testing.T) {
	// Arrange.
	assert := assert.New(t)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[0].ID, strings.NewReader("{key:value}"))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusBadRequest, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonJSONInvalid,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithValidationFailed(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	invalidHardwarePlatform := hardwarePlatforms[0]
	invalidHardwarePlatform.TargetName = "ESP32"
	payloadBytes, err := json.Marshal(invalidHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+invalidHardwarePlatform.ID, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusBadRequest, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonValidationFailed,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithConflict(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)
	_, err = hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[1])
	assert.Nil(err)
	updatedHardwarePlatform := hardwarePlatforms[1]
	updatedHardwarePlatform.TargetName = hardwarePlatforms[0].TargetName
	payloadBytes, err := json.Marshal(updatedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[1].ID, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusConflict, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonEntityExists,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithTrimming(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)
	untrimmedHardwarePlatform := hardwarePlatforms[0]
	untrimmedHardwarePlatform.Model = "      ESP32-S0      "
	payloadBytes, err := json.Marshal(untrimmedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[0].ID, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusOK, res.StatusCode)
	var body oneResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	expectedBody := oneResponse{Data: untrimmedHardwarePlatform}
	expectedBody.Data.Model = "ESP32-S0"
	expectedBody.Data.CreatedAt = body.Data.CreatedAt
	assert.Equal(expectedBody, body)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: body.Data.ID}}).Decode(&doc)
	doc.OID = primitive.ObjectID{}
	assert.Nil(err)
	assert.Equal(expectedBody.Data, doc)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestUpdateHardwarePlatformWithEntityUnknown(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	updatedHardwarePlatform := hardwarePlatforms[0]
	updatedHardwarePlatform.Model = "ESP32-S0"
	payloadBytes, err := json.Marshal(updatedHardwarePlatform)
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodPut, url+"/"+hardwarePlatforms[0].ID, bytes.NewReader(payloadBytes))
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusNotFound, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonEntityUnknown,
	}, body)

	// Teardown.
	_, err = hardwarePlatformsCollection.DeleteMany(context.Background(), bson.D{})
	assert.Nil(err)
}

func TestDeleteHardwarePlatformWithID(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodDelete, url+"/"+hardwarePlatforms[0].ID, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusNoContent, res.StatusCode)
	n, err := new(bytes.Buffer).ReadFrom(res.Body)
	assert.Nil(err)
	assert.Equal(int64(0), n)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: hardwarePlatforms[0].ID}}).Decode(&doc)
	assert.Equal(mongo.ErrNoDocuments, err)
}

func TestDeleteHardwarePlatformWithTargetName(t *testing.T) {
	// Arrange.
	assert := assert.New(t)
	_, err := hardwarePlatformsCollection.InsertOne(context.Background(), hardwarePlatforms[0])
	assert.Nil(err)

	// Act.
	req, err := http.NewRequest(http.MethodDelete, url+"/"+hardwarePlatforms[0].TargetName, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusNoContent, res.StatusCode)
	n, err := new(bytes.Buffer).ReadFrom(res.Body)
	assert.Nil(err)
	assert.Equal(int64(0), n)
	var doc models.HardwarePlatform
	err = hardwarePlatformsCollection.FindOne(context.Background(), bson.D{bson.E{Key: "id", Value: hardwarePlatforms[0].ID}}).Decode(&doc)
	assert.Equal(mongo.ErrNoDocuments, err)
}

func TestDeleteHardwarePlatformWithEntityUnknown(t *testing.T) {
	// Arrange.
	assert := assert.New(t)

	// Act.
	req, err := http.NewRequest(http.MethodDelete, url+"/"+hardwarePlatforms[0].TargetName, nil)
	assert.Nil(err)
	res, err := app.Test(req)
	assert.Nil(err)

	// Assert.
	assert.Equal(http.StatusNotFound, res.StatusCode)
	var body errorResponse
	err = json.NewDecoder(res.Body).Decode(&body)
	assert.Nil(err)
	assert.Equal(errorResponse{
		Error: util.ReasonEntityUnknown,
	}, body)
}
