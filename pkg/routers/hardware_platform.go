package routers

import (
	"context"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"

	"gitlab.com/paperstack-org/device-registry/pkg/middlewares"
	"gitlab.com/paperstack-org/device-registry/pkg/models"
	"gitlab.com/paperstack-org/device-registry/pkg/util"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var hardwarePlatformsCollection *mongo.Collection

// ListHardwarePlatforms fetches all entities of this resource.
// Commonly referred to as FIND operation.
func ListHardwarePlatforms(c *fiber.Ctx) error {
	// Get request context.
	ctx := c.Context()

	// Run paginated database query.
	query, opts, pagination := util.Paginate(c)
	cursor, err := hardwarePlatformsCollection.Find(ctx, query, opts)
	if err != nil {
		return err
	}

	// Decode queried data.
	hardwarePlatforms := make([]models.HardwarePlatform, 0)
	if err := cursor.All(ctx, &hardwarePlatforms); err != nil {
		return err
	}

	// Set the cursor for the next page.
	count := int64(len(hardwarePlatforms))
	if count > 0 {
		pagination.Next = hardwarePlatforms[count-1].OID.Hex()
	} else {
		pagination.Next = ""
	}

	// Send response.
	return c.Status(http.StatusOK).JSON(util.DataBody{
		Data:       hardwarePlatforms,
		Pagination: pagination,
	})
}

// CreateHardwarePlatform creates an entity of this resource.
// Commonly referred to as CREATE operation.
func CreateHardwarePlatform(c *fiber.Ctx) error {
	// Decode request body.
	hardwarePlatform := new(models.HardwarePlatform)
	if err := c.BodyParser(hardwarePlatform); err != nil {
		return err
	}

	// Validate entity.
	if err := hardwarePlatform.Validate(true); err != nil {
		return err
	}

	// Create new entity.
	res, err := hardwarePlatformsCollection.InsertOne(c.Context(), hardwarePlatform)
	if err != nil {
		return err
	}
	hardwarePlatform.OID = res.InsertedID.(primitive.ObjectID)

	// Send response.
	return c.Status(http.StatusCreated).JSON(util.DataBody{
		Data: *hardwarePlatform,
	})
}

// ReadHardwarePlatform creates an entity of this resource.
// Commonly referred to as READ operation.
func ReadHardwarePlatform(c *fiber.Ctx) error {
	// Get the entity ID.
	idOrTargetName := c.Params("id")

	// Attempt to parse UUID.
	query := bson.D{bson.E{Key: "target_name", Value: idOrTargetName}}
	if _, err := uuid.Parse(idOrTargetName); err == nil {
		query = bson.D{bson.E{Key: "id", Value: idOrTargetName}}
	}

	// Read existing entity.
	hardwarePlatform := new(models.HardwarePlatform)
	if err := hardwarePlatformsCollection.FindOne(c.Context(), query).Decode(hardwarePlatform); err != nil {
		return err
	}

	// Send response.
	return c.Status(http.StatusOK).JSON(util.DataBody{
		Data: hardwarePlatform,
	})
}

// UpdateHardwarePlatform updates an entity of this resource.
// Commonly referred to as UPDATE operation.
func UpdateHardwarePlatform(c *fiber.Ctx) error {
	// Decode request body.
	hardwarePlatform := new(models.HardwarePlatform)
	if err := c.BodyParser(hardwarePlatform); err != nil {
		return err
	}

	// Validate entity.
	if err := hardwarePlatform.Validate(false); err != nil {
		return err
	}

	// Get the entity ID.
	idOrTargetName := c.Params("id")

	// Attempt to parse UUID.
	query := bson.D{bson.E{Key: "target_name", Value: idOrTargetName}}
	if _, err := uuid.Parse(idOrTargetName); err == nil {
		query = bson.D{bson.E{Key: "id", Value: idOrTargetName}}
	}

	// Update existing entity.
	opts := options.FindOneAndReplace().SetReturnDocument(options.After)
	if err := hardwarePlatformsCollection.FindOneAndReplace(c.Context(), query, hardwarePlatform, opts).Decode(hardwarePlatform); err != nil {
		return err
	}

	// Send response.
	return c.Status(http.StatusOK).JSON(util.DataBody{
		Data: hardwarePlatform,
	})
}

// DeleteHardwarePlatform deletes an entity of this resource.
// Commonly referred to as DELETE operation.
func DeleteHardwarePlatform(c *fiber.Ctx) error {
	// Get the entity ID.
	idOrTargetName := c.Params("id")

	// Attempt to parse UUID.
	query := bson.D{bson.E{Key: "target_name", Value: idOrTargetName}}
	if _, err := uuid.Parse(idOrTargetName); err == nil {
		query = bson.D{bson.E{Key: "id", Value: idOrTargetName}}
	}

	// Delete existing entity.
	hardwarePlatform := new(models.HardwarePlatform)
	if err := hardwarePlatformsCollection.FindOneAndDelete(c.Context(), query).Decode(hardwarePlatform); err != nil {
		return err
	}

	return c.SendStatus(http.StatusNoContent)
}

// HardwarePlatform returns a router for the resource.
func HardwarePlatform() *fiber.App {
	hardwarePlatformsCollection = util.Connect().Collection("hardware-platforms")
	app := fiber.New()

	// Create device indexes.
	opts := options.CreateIndexes().SetMaxTime(2 * time.Second)
	_, err := hardwarePlatformsCollection.Indexes().CreateMany(context.Background(), models.HardwarePlatformIndexes, opts)
	if err != nil {
		log.Fatal().Err(err).Msg("Creating indexes failed")
	}

	// Configure route handlers.
	app.Get("/", ListHardwarePlatforms)

	app.Post("/", middlewares.AuthZNoAnonymous())
	app.Post("/", CreateHardwarePlatform)

	app.Get("/:id", ReadHardwarePlatform)

	app.Put("/:id", middlewares.AuthZNoAnonymous())
	app.Put("/:id", UpdateHardwarePlatform)

	app.Delete("/:id", middlewares.AuthZNoAnonymous())
	app.Delete("/:id", DeleteHardwarePlatform)

	return app
}
