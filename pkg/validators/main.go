package validators

import (
	"regexp"
)

var ecPrivateKeyRegexp = regexp.MustCompile("^-----BEGIN EC PRIVATE KEY-----[A-Za-z0-9+/=\n]*-----END EC PRIVATE KEY-----\n?$")
var mongoIDRegexp = regexp.MustCompile("^[0-9a-f]{24}$")

// IsEcPrivateKey returns true if the string
// is a valid EC private key in PEM format.
func IsEcPrivateKey(privateKey string) bool {
	return ecPrivateKeyRegexp.MatchString(privateKey)
}

// IsMongoID returns true if the string
// is a valid MongoDB ID.
func IsMongoID(mongoID string) bool {
	return mongoIDRegexp.MatchString(mongoID)
}
