package config

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

var (
	service = "demo"
	version = "dev"
)

var config *Config

const (
	// EnvironmentLocalhost is the application configuration during development.
	EnvironmentLocalhost = "localhost"
	// EnvironmentProduction is the application configuration for production environments.
	EnvironmentProduction = "production"
	// IdentityFormatECPrime256V1PEM is the identity format for a PEM encoded EC prime-256v1 curve keypair.
	IdentityFormatECPrime256V1PEM = "EC_PRIME256V1_PEM"
	// IdentityFormatRSAPEM is the identity format for a PEM encoded RSA keypair.
	IdentityFormatRSAPEM = "RSA_PEM"
)

// Config is the application configuration.
type Config struct {
	Service                   string
	Version                   string
	Hostname                  string
	Port                      string
	MongoURI                  string
	Database                  string
	RootDomain                string
	ServiceURI                string
	StartedAt                 string
	ContentType               string
	CertDir                   string
	Environment               string
	ServiceIdentity           ServiceIdentity
	ServiceIdentityPrivateKey crypto.PrivateKey
}

// ServiceIdentity is the identity of a service.
type ServiceIdentity struct {
	Format     string `json:"format"`
	PublickKey string `json:"public_key"`
}

// Load loads the configuration.
func Load() *Config {
	if config != nil {
		return config
	}

	err := godotenv.Load()
	if err != nil {
		log.Info().Msg("File not found: .env")
	}

	startedAt := time.Now().UTC().Format(time.RFC3339)
	rootDomain := GetenvMandatory("ROOT_DOMAIN")
	port := GetenvOptional("PORT", "8080")
	certDir := path.Clean(GetenvMandatory("CERT_DIR"))
	contentType := "application/json; charset=utf-8"
	environment := GetenvOptional("ENVIRONMENT", EnvironmentProduction)

	// Check if application environment is configured correctly.
	if environment != EnvironmentLocalhost && environment != EnvironmentProduction {
		log.Fatal().Err(err).Msg("Environment variable invalid: ENVIRONMENT")
	}

	// Configure service URI based on application environment.
	serviceURI := "https://" + service + "." + rootDomain
	if environment == EnvironmentLocalhost {
		serviceURI = "http://" + service + "." + rootDomain + ":" + port
	}

	// Obtain the network hostname of the machine.
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal().Err(err).Msg("Reading hostname failed")
	}

	// Parse the MongoDB URI.
	mongoURI := GetenvMandatory("MONGO_URI")
	parsedMongoURI, err := url.Parse(mongoURI)
	if err != nil {
		log.Fatal().Err(err).Msg("Environment variable invalid: MONGO_URI")
	}
	database := strings.TrimLeft(parsedMongoURI.Path, "/")

	// Load the service identity.
	serviceIdentityPrivateKey, serviceIdentity := loadServiceIdentity(certDir)

	config = &Config{
		Service:                   service,
		Version:                   version,
		Hostname:                  hostname,
		Port:                      port,
		MongoURI:                  mongoURI,
		Database:                  database,
		RootDomain:                rootDomain,
		ServiceURI:                serviceURI,
		StartedAt:                 startedAt,
		ContentType:               contentType,
		CertDir:                   certDir,
		Environment:               environment,
		ServiceIdentity:           serviceIdentity,
		ServiceIdentityPrivateKey: serviceIdentityPrivateKey,
	}

	return config
}

func loadServiceIdentity(certDir string) (crypto.PrivateKey, ServiceIdentity) {
	privateKeyFile := path.Join(certDir, "id.key")
	publicKeyFile := path.Join(certDir, "id.pub")

	// Load PEM encoded private key file.
	privateKeyBytes, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		log.Fatal().Err(err).Msg("Loading service identity private key failed: " + privateKeyFile)
	}
	privateKeyBlock, _ := pem.Decode(privateKeyBytes)

	// Load PEM encoded public key file.
	publicKeyBytes, err := ioutil.ReadFile(publicKeyFile)
	if err != nil {
		log.Fatal().Err(err).Msg("Loading service identity public key failed: " + publicKeyFile)
	}
	publicKeyBlock, _ := pem.Decode(publicKeyBytes)
	publicKey, err := x509.ParsePKIXPublicKey(publicKeyBlock.Bytes)
	if err != nil {
		log.Fatal().Err(err).Msg("Parsing service identity public key failed: " + publicKeyFile)
	}

	// Parse EC private key.
	if privateKeyBlock.Type == "EC PRIVATE KEY" {
		ecPrivateKey, err := x509.ParseECPrivateKey(privateKeyBlock.Bytes)
		if err != nil {
			log.Fatal().Err(err).Msg("Parsing service identity private key failed: " + privateKeyFile)
		}
		ecPrivateKey.PublicKey = *publicKey.(*ecdsa.PublicKey)
		return ecPrivateKey, ServiceIdentity{
			PublickKey: strings.TrimSpace(string(publicKeyBytes)),
			Format:     IdentityFormatECPrime256V1PEM,
		}
	}

	rsaPrivateKey, err := x509.ParsePKCS1PrivateKey(privateKeyBlock.Bytes)
	if err != nil {
		log.Fatal().Err(err).Msg("Parsing service identity private key failed: " + privateKeyFile)
	}
	rsaPrivateKey.PublicKey = *publicKey.(*rsa.PublicKey)
	return rsaPrivateKey, ServiceIdentity{
		PublickKey: strings.TrimSpace(string(publicKeyBytes)),
		Format:     IdentityFormatRSAPEM,
	}
}
