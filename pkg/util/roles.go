package util

var (
	// RoleAnonymous is the role if a client does not provide credentials.
	RoleAnonymous = "anonymous"
	// RoleTrustedService is the role of a service that shares the trusted root domain.
	RoleTrustedService = "trusted_service"
)
