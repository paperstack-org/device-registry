package util

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/paperstack-org/device-registry/pkg/validators"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Paginate parses the offset and limit query parameters and paginates the results.
func Paginate(c *fiber.Ctx) (primitive.D, *options.FindOptions, Pagination) {
	// Parse limit from request query.
	limit, err := strconv.ParseInt(c.Query("limit"), 10, 64)
	if err != nil || limit < 1 {
		limit = 20
	}
	if limit > 100 {
		limit = 100
	}

	// Parse next from request query.
	next := c.Query("next")

	// Configure find options.
	opts := options.Find().SetSort(bson.D{
		bson.E{Key: "created_at", Value: -1},
		bson.E{Key: "_id", Value: -1},
	}).SetLimit(limit)

	// Assemble database query.
	query := bson.D{}
	if validators.IsMongoID(next) {
		oid, err := primitive.ObjectIDFromHex(next)
		if err != nil {
			return query, opts, Pagination{
				Limit: limit,
				Next:  "",
			}
		}
		query = bson.D{bson.E{Key: "_id", Value: bson.D{bson.E{Key: "$lt", Value: oid}}}}
	}

	return query, opts, Pagination{
		Limit: limit,
		Next:  next,
	}
}
