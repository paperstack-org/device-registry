package util

import (
	"context"

	"github.com/rs/zerolog/log"

	"gitlab.com/paperstack-org/device-registry/pkg/config"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var client *mongo.Client
var database *mongo.Database

// Connect creates a connection to the MongoDB database and ensure that the server is reachable.
func Connect() *mongo.Database {
	if database != nil {
		return database
	}

	cfg := config.Load()
	ctx := context.Background()

	// Connect to database.
	var err error
	client, err = mongo.Connect(ctx, options.Client().ApplyURI(cfg.MongoURI))
	if err != nil {
		log.Fatal().Err(err).Msg("Connecting to database failed: " + cfg.Database)
	}

	// Check if database is available.
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal().Err(err).Msg("Pinging database failed: " + cfg.Database)
	}

	database = client.Database(cfg.Database)

	return database
}
