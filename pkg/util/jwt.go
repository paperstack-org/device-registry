package util

import (
	"crypto/ecdsa"
	"crypto/rsa"
	"io/ioutil"
	"os"
	"path"

	"github.com/cristalhq/jwt/v3"
	"github.com/rs/zerolog/log"

	"gitlab.com/paperstack-org/device-registry/pkg/config"
)

var signer jwt.Signer
var builder *jwt.Builder

const (
  // IdentityTypeTrustedService that will cause the validation to use the service health endpoint.
  IdentityTypeTrustedService = "trusted_service"
)

// ServiceClaims contains additional information about the token subject.
type ServiceClaims struct {
  jwt.StandardClaims
  IdentityType string `json:"identity_type"`
}

// NewServiceToken creates a new service token (JWT) for the specified audience.
func NewServiceToken(audience []string) string {
	cfg := config.Load()

	// Configure the signer if it is not yet initialized.
	if signer == nil {
		var err error

		switch cfg.ServiceIdentity.Format {
		case "EC_PRIME256V1_PEM":
			privateKey := cfg.ServiceIdentityPrivateKey.(*ecdsa.PrivateKey)
			signer, err = jwt.NewSignerES(jwt.ES512, privateKey)
		case "RSA_PEM":
			privateKey := cfg.ServiceIdentityPrivateKey.(*rsa.PrivateKey)
			signer, err = jwt.NewSignerRS(jwt.RS512, privateKey)
		default:
			log.Fatal().Msg("Unsupported identity format: " + cfg.ServiceIdentity.Format)
		}

		if err != nil {
			log.Fatal().Err(err).Msg("Creating signer failed: " + cfg.ServiceIdentity.Format)
		}
	}

	// Configure the builder if it is not yet initialized.
	if builder == nil {
		builder = jwt.NewBuilder(signer)
	}

	// Set the JWT claims.
	claims := &ServiceClaims{
    StandardClaims: jwt.StandardClaims{
      Audience: audience,
      Subject:  cfg.ServiceURI,
      Issuer:   cfg.ServiceURI,
    },
    IdentityType: IdentityTypeTrustedService,
	}

	// Generate a service token.
	token, err := builder.Build(claims)
	if err != nil {
		log.Fatal().Err(err).Msg("Generating service identity token failed")
	}

	return token.String()
}

// WriteServiceToken creates a file in the temporary directory to
// store a service token that can be used to access only this service.
func WriteServiceToken() {
	cfg := config.Load()

	// Generate new admin token.
	serviceToken := NewServiceToken([]string{cfg.ServiceURI})

	// Write admin token to temporary directory.
	serviceTokenFile := path.Join(os.TempDir(), cfg.Service+".jwt")
	err := ioutil.WriteFile(serviceTokenFile, []byte(serviceToken), 0600)
	if err != nil {
		log.Fatal().Err(err).Msg("Writing service token failed")
	}
	log.Info().Msg("Wrote service token: " + serviceTokenFile)
}
