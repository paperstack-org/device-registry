package util

import (
	"github.com/gofiber/fiber/v2"
)

// Pagination describes the pagination parameters used for a query.
type Pagination struct {
	Next  string `json:"next"`
	Limit int64  `json:"limit"`
}

// ErrorBody describes the HTTP response body when an error is sent to the client.
type ErrorBody struct {
	Error Reason `json:"error"`
}

// DataBody describes the HTTP response body when data is sent to the client.
type DataBody struct {
	Data       interface{} `json:"data"`
	Pagination interface{} `json:"pagination,omitempty"`
}

// SendError sends an HTTP error by using the fiber context.
func SendError(c *fiber.Ctx, reason *Reason) error {
	// Send reason.
	return c.Status(reason.Status).JSON(ErrorBody{
		Error: *reason,
	})
}
