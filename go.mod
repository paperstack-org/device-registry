module gitlab.com/paperstack-org/device-registry

go 1.15

require (
	github.com/cristalhq/jwt/v3 v3.0.4
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gofiber/fiber/v2 v2.2.0
	github.com/gofiber/helmet/v2 v2.0.1
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.3
)
