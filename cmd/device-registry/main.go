package main

import (
	"gitlab.com/paperstack-org/device-registry/pkg/app"
)

func main() {
	app.Start()
}
