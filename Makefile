.PHONY: run test build deps download certs

REMOTE=gitlab.com
GROUP=paperstack-org
PROJECT=device-registry
VERSION?=dev
# The CPU architecture of the build host.
MACHINE?=$(shell uname -m)
# The CPU architecture of the target host. This may be one of "arm64", "armv7" or "amd64". Defaults to "amd64".
ARCH?=amd64
BRANCH?=$(shell git rev-parse --abbrev-ref HEAD)

# Set GOARCH and GOARM values for cross-compilation.
ifeq ($(ARCH),amd64)
	GOARCH=amd64
endif

ifeq ($(ARCH),armv7)
	GOARCH=arm
	GOARM=7
endif

ifeq ($(ARCH),arm64)
	GOARCH=arm64
endif

run:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go run -ldflags "-X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.version=$(VERSION) -X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.service=$(PROJECT)" cmd/$(PROJECT)/main.go

test:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go test ./... -cover -v

build:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go build -ldflags "-X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.version=$(VERSION) -X $(REMOTE)/$(GROUP)/$(PROJECT)/pkg/config.service=$(PROJECT)" -o $(PROJECT)-$(ARCH) cmd/$(PROJECT)/main.go
ifeq ($(MACHINE),x86_64)
	upx -9 $(PROJECT)-$(ARCH)
endif

deps:
	CGO_ENABLED=0 GOARCH=$(GOARCH) GOARM=$(GOARM) go mod download

download:
	wget -q --show-progress https://$(REMOTE)/$(GROUP)/$(PROJECT)/-/jobs/artifacts/$(BRANCH)/raw/$(PROJECT)-$(ARCH)?job=compile-$(ARCH) -O $(PROJECT)-$(ARCH)
	chmod +x $(PROJECT)-$(ARCH)

certs:
	mkdir -p certs
	openssl ecparam -genkey -name prime256v1 -noout -out certs/id.key 2> /dev/null
	openssl ec -in certs/id.key -pubout -out certs/id.pub 2> /dev/null
